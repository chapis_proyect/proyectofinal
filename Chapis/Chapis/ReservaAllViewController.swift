//
//  ReservaAllViewController.swift
//  Chapis
//
//  Created by Laboratorio FIS on 23/2/18.
//  Copyright © 2018 Laboratorio FIS. All rights reserved.
//

import UIKit
import CoreData

class ReservaAllViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {

    var persona: Persona?
    var ReservaIndex = 0
    var ReservaArray:[Reserva] = []
    
    let manageObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
        persona = myTabBar?.person
        mostrarTodos()
        // Do any additional setup after loading the view.
    }
    
    var myTabBar: StabBarViewController? {
        return tabBarController as? StabBarViewController
    }
    
    func mostrarTodos() {

        let entityDescription = NSEntityDescription.entity(forEntityName: "Persona", in: manageObjectContext)
        let request:NSFetchRequest<Persona> = Persona.fetchRequest()
        request.entity = entityDescription
        let predicate = NSPredicate(format: "usuario = %@", (persona?.usuario!)!)
        request.predicate = predicate
        
        do {
            let results = try(manageObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>))
            if results.count > 0 {
                let p = results.first! as! Persona
                //print("Encontrada una persona: \(p.nombre!) - \(p.usuario!) con \(p.personaReserva!.count)")
                if let reservas_de_p = p.personaReserva {
                    for c in reservas_de_p {
                        let c = c as! Reserva
                        ReservaArray.append(c)
                        print("Coche de \(p.nombre!): \(c.numero_personas) - \(c.fecha_hora!)")
                    }
                }
            } else {
                print("No hay personas.")
            }
        } catch let error as NSError {
            print("Error al recuperar: \(error)")
        }
    }

    @IBAction func AgregarReservaButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "agregarReservaSegue", sender: self)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ReservaArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReservasTodas") as! TableViewCell
        cell.fillCell(reserva: ReservaArray[indexPath.row])
        return cell
    }
    
    /*func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        switch section {
        case 0:
            return "sección 1"
        default:
            return "sección 2"
        }
    }*/
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "agregarReservaSegue" {
            let destination = segue.destination as! AgregarReservaViewController
            destination.PersonaRes = persona
        }
    }
}
