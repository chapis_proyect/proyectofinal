//
//  ModificarPerfilViewController.swift
//  Chapis
//
//  Created by Laboratorio FIS on 23/2/18.
//  Copyright © 2018 Laboratorio FIS. All rights reserved.
//

import UIKit
import CoreData

class ModificarPerfilViewController: UIViewController {

     var personaM: Persona?
    
    @IBOutlet weak var usuario: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var celular: UITextField!
    @IBOutlet weak var contrasena: UITextField!
    @IBOutlet weak var confirmarPasswordTextField: UITextField!
    
    let manageObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
        usuario.text = personaM?.usuario
        email.text = personaM?.email
        celular.text = personaM?.celular
        contrasena.text = personaM?.contrasena

        // Do any additional setup after loading the view.
    }

    var myTabBar: StabBarViewController? {
        return tabBarController as? StabBarViewController
    }
    
    private func mostrarAlerta(mensaje:String){
        let alertView = UIAlertController(title: "Notificación", message: mensaje , preferredStyle: .alert)
        
        let aceptar = UIAlertAction(title: "Aceptar", style: .default){
            (action)in
        }
        
        alertView.addAction(aceptar)
        
        present(alertView, animated : true, completion:nil)
    }
    
    func modificar(p: Persona) {

        p.usuario = usuario.text ?? ""
        p.email = email.text ?? ""
        p.celular = celular.text ?? ""
        p.contrasena = contrasena.text ?? ""
        
        do {
            try manageObjectContext.save()
            //showMessage()
            //clearFields()
            mostrarAlerta(mensaje: "Sus Datos han sido modificados")
        } catch let error as NSError {
            print("Error al modificar: \(error)")
        }
    }
    
    func showMessage() {
        let alert = "dfjnd"
        print (alert)
    }
    
    func clearFields(){
        
        usuario.text = ""
        email.text = ""
        celular.text = ""
        contrasena.text = ""
        confirmarPasswordTextField.text = ""
    }
    
    @IBAction func modificarButton(_ sender: Any) {
        modificar(p: personaM!)
    }
}
