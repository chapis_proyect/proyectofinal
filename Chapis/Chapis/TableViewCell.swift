//
//  TableViewCell.swift
//  Chapis
//
//  Created by Laboratorio FIS on 27/2/18.
//  Copyright © 2018 Laboratorio FIS. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {


    @IBOutlet weak var NumeroPersonas: UILabel!
    @IBOutlet weak var horaFechaReserva: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func fillCell(reserva: Reserva) {
        
        NumeroPersonas.text = "\(reserva.numero_personas)"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
        let HFaux = dateFormatter.string(from: reserva.fecha_hora!)
        horaFechaReserva.text = HFaux
    }
}
