//
//  ResgitroViewController.swift
//  Chapis
//
//  Created by Laboratorio FIS on 14/2/18.
//  Copyright © 2018 Laboratorio FIS. All rights reserved.
//

import UIKit
import CoreData

class ResgitroViewController: UIViewController {

    
    @IBOutlet weak var nombreText: UITextField!
    @IBOutlet weak var usuarioText: UITextField!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var celularText: UITextField!
    @IBOutlet weak var contraText: UITextField!
    @IBOutlet weak var confContraText: UITextField!
    
    let manageObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    //var singlePerson: Persona?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    func savePerson() {
        
        let entityDescription = NSEntityDescription.entity(forEntityName: "Persona", in: manageObjectContext)
        
        let persona = Persona(entity: entityDescription!, insertInto: manageObjectContext)
        
        persona.nombre = nombreText.text ?? ""
        persona.usuario = usuarioText.text ?? ""
        persona.email = emailText.text ?? ""
        persona.celular = celularText.text ?? ""
        persona.contrasena = contraText.text ?? ""
        
        if (contraText.text == confContraText.text) {
            do {
                try manageObjectContext.save()
                showMessage()
                clearFields()
            } catch {
                print("Error")
            }
        }
    }
    
    func showMessage() {
        let alert = "dfjnd"
        print (alert)
    }
    
    
    func clearFields(){
        
        nombreText.text = ""
        usuarioText.text = ""
        emailText.text = ""
        celularText.text = ""
        contraText.text = ""
        confContraText.text = ""
    }
    @IBAction func Button_SAve(_ sender: Any) {
        savePerson()
    }
}
