//
//  StabBarViewController.swift
//  Chapis
//
//  Created by Mayra Rosero on 2/22/18.
//  Copyright © 2018 Laboratorio FIS. All rights reserved.
//

import UIKit

class StabBarViewController: UITabBarController {

    var person: Persona?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        let left = tabBar.items![0]
        let medio1 = tabBar.items![1]
        let medio2 = tabBar.items![2]
        let rigth = tabBar.items![3]
        
        //left.image = #imageLiteral(resourceName: "ic_motorcycle")
        //medio.image = #imageLiteral(resourceName: "ic_receipt")
        //rigth.image = #imageLiteral(resourceName: "ic_motorcycle")
        left.title = "Inicio"
        medio1.title = "Menú"
        medio2.title = "Perfil"
        rigth.title = "Reservas"
        
    
        left.image = #imageLiteral(resourceName: "ic_restaurant_menu")
        medio1.image = #imageLiteral(resourceName: "ic_restaurant_menu")
        medio2.image = #imageLiteral(resourceName: "ic_account_box")
        rigth.image = #imageLiteral(resourceName: "ic_event_note")
        
    }
}
