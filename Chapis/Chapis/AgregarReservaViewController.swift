//
//  AgregarReservaViewController.swift
//  Chapis
//
//  Created by Laboratorio FIS on 23/2/18.
//  Copyright © 2018 Laboratorio FIS. All rights reserved.
//

import UIKit
import CoreData

class AgregarReservaViewController: UIViewController {

    @IBOutlet weak var numPersonasText: UITextField!
    @IBOutlet weak var fechaLabel: UIDatePicker!
    
    var PersonaRes : Persona?
    var strDate : String?
    
    let manageObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func RegistrarButtonPRessed(_ sender: Any) {
        
        print (self.numPersonasText.text ?? "")
        //print(self.)
        //fec = self.fechaLabel.text
        datePickerAction((Any).self)
        print(strDate!)
        saveReserva(p: PersonaRes!)
    }
    
    @IBAction func datePickerAction(_ sender: Any) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
        strDate = dateFormatter.string(from: fechaLabel.date)
    }
    
    
    func saveReserva(p: Persona) {
        
        let entityDescription = NSEntityDescription.entity(forEntityName: "Reserva", in: manageObjectContext)
        
        let reserva = Reserva(entity: entityDescription!, insertInto: manageObjectContext)
        let auxNP: Int16? = Int16(numPersonasText.text!)
        reserva.numero_personas = auxNP!
        reserva.fecha_hora = fechaLabel.date
        reserva.reservaPersona = p
        
        do {
            try manageObjectContext.save()
            showMessage()
            clearFields()
        } catch {
            print("Error")
        }
    }
    
    @IBAction func mostrarPrueba(_ sender: Any) {
        
        let entityDescription = NSEntityDescription.entity(forEntityName: "Persona", in: manageObjectContext)
        let request:NSFetchRequest<Persona> = Persona.fetchRequest()
        request.entity = entityDescription
        let predicate = NSPredicate(format: "usuario = %@", (PersonaRes?.usuario!)!)
        request.predicate = predicate
        
        do {
            let results = try(manageObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>))
            if results.count > 0 {
                let p = results.first! as! Persona
                print("Encontrada una persona: \(p.nombre!) - \(p.usuario!) con \(p.personaReserva!.count)")
                if let coches_de_p = p.personaReserva {
                    for c in coches_de_p {
                        let c = c as! Reserva
                        print("Coche de \(p.nombre!): \(c.numero_personas) - \(c.fecha_hora!)")
                    }
                }
                //return p
            } else {
                print("No hay personas.")
                //return nil
            }
        } catch let error as NSError {
            print("Error al recuperar: \(error)")
        }
        
        //return nil
    }
    
    func showMessage() {
        let alert = "SI"
        print (alert)
    }
    
    
    func clearFields(){
        
        numPersonasText.text = ""
    }
}
