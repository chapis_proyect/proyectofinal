//
//  ViewController.swift
//  Chapis
//
//  Created by Laboratorio FIS on 7/2/18.
//  Copyright © 2018 Laboratorio FIS. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
    @IBOutlet weak var UIuser: UITextField!
    @IBOutlet weak var uiPass: UITextField!
    let manageObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var singlePerson: Persona?
    let valorPsw = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        UIuser.text = ""
        uiPass.text = ""
        UIuser.becomeFirstResponder()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        
    }
    //Mark:- Actions
    @IBAction func BotonEntrar(_ sender: Any) {
        //let user = UIuser.text ?? ""
        let pass = uiPass.text ?? ""
        
        let entityDescription = NSEntityDescription.entity(forEntityName: "Persona", in: manageObjectContext)
        let request:NSFetchRequest<Persona> = Persona.fetchRequest()
        request.entity = entityDescription
        let predicate = NSPredicate(format: "usuario = %@", UIuser.text!)
        request.predicate = predicate
        
        do {
            
            let results = try(manageObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>))
            if results.count > 0 {
                let match = results[0] as! Persona
                singlePerson = match
                //uiPass.text = match.contrasena
                if (pass == match.contrasena) {
                    print("login correcto")
                    //performSegue(withIdentifier: "CiudadSegue", sender: self)
                    performSegue(withIdentifier: "ingresarSegue", sender: self)
                    
                } else {
                    mostrarAlerta(mensaje:"usuario o contraseña incorrectas")
                }
                
            } else {
                //uiPass.text = "n/a"
                mostrarAlerta(mensaje:"usuario o contraseña incorrectas")
        }
            
        }catch{
            print("Error finding one")
        }
        
    }
    
    @IBAction func RegistrarButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "registrarSegue", sender: self)
    }
    
    private func mostrarAlerta(mensaje:String){
        let alertView = UIAlertController(title: "error", message: mensaje , preferredStyle: .alert)
        
        let aceptar = UIAlertAction(title: "Aceptar", style: .default){
            (action)in
            self.uiPass.text=""
            self.UIuser.text=""
        }
        alertView.addAction(aceptar)
        
        present(alertView, animated : true, completion:nil)
    }
    
    func findone() {
        let entityDescription = NSEntityDescription.entity(forEntityName: "Persona", in: manageObjectContext)
        let request:NSFetchRequest<Persona> = Persona.fetchRequest()
        request.entity = entityDescription
        let predicate = NSPredicate(format: "usuario = %@", UIuser.text!)
        request.predicate = predicate
        
        do {
            
            let results = try(manageObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>))
            if results.count > 0 {
                let match = results[0] as! Persona
                singlePerson = match
                //performSegue(withIdentifier: "findOneSegue", sender: self)
                uiPass.text = match.contrasena
                //nameTextField.text = match.name
                // phoneTextfield.text = match.phone
            } else {
                uiPass.text = "n/a"
                //nameTextField.text = "n/a"
                //phoneTextfield.text = "n/a"
            }
            
        }catch{
            print("Error finding one")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "registrarSegue" {
            let destination = segue.destination as! ResgitroViewController
            
        }
        if segue.identifier == "ingresarSegue" {
            let destination = segue.destination as! StabBarViewController
            destination.person = singlePerson
        }
    }
}

