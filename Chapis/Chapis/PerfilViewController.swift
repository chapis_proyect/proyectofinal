//
//  PerfilViewController.swift
//  Chapis
//
//  Created by Laboratorio FIS on 23/2/18.
//  Copyright © 2018 Laboratorio FIS. All rights reserved.
//

import UIKit

class PerfilViewController: UIViewController {

    var persona: Persona?
    
    @IBOutlet weak var nombreLabelP: UITextField!
    @IBOutlet weak var usuarioLabelP: UITextField!
    @IBOutlet weak var mailLabelP: UITextField!
    @IBOutlet weak var celularLabelP: UITextField!
    
    var myTabBar: StabBarViewController? {
        return tabBarController as? StabBarViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        persona = myTabBar?.person
        nombreLabelP.text = persona?.nombre
        usuarioLabelP.text = persona?.usuario
        mailLabelP.text = persona?.email
        celularLabelP.text = persona?.celular
        /*title = person?.name
        addressLabel.text = person?.adress!
        phoneLabel.text = person?.phone*/
    }
    
    @IBAction func ModificarButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "modificarPerfilSegue", sender: self)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "modificarPerfilSegue" {
            let destination = segue.destination as! ModificarPerfilViewController
            destination.personaM = persona
            
        }
    }
}
